# Software Project

## The Legend of Sop

It is a single player top-down RPG pixel art game.  It is a team work for Software Project university course.



### Build Instruction

**Linux & MacOS**

```
$ git clone https://gitlab.com/software-project-team/software-project.git
$ cd software-project
$ make
$ ./software_project
```

**Windows**

For windows `MinGW` is needed. Also run `mingw32-make.exe`  instead of `make` and run `./software_project.exe`



### Architecture

**Programming language: **C++
**Game library: **raylib v4.0.0.0
**Physics engine:** Box2D v2.4.1


**Other Libraries:**

- EnTT

- json.nlohmann.me

- raymath



**Used technologies:**

- Blender

- Python

- Python Pillow

- ImageMagick

- Tiled

- LibreSprite



