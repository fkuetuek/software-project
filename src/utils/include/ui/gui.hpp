/*
 * gui.hpp
 *
 *  Created on: Nov 24, 2021
 *      Author: nasim
 */

#ifndef SRC_UTILS_INCLUDE_GUI_GUI_HPP_
#define SRC_UTILS_INCLUDE_GUI_GUI_HPP_

#include <iostream>
#include "raylib.h"
#include "color_scheme.hpp"


namespace spgu {
namespace ui {
class gui;
}
}
/**
 * @brief A class for defining greaphical user interface
 * @author ysf
 *
 * This is a simple class for creating different types of buttons and checkbox.
 */
class spgu::ui::gui {
public:
	/**
	 * Constructor that creates the graphical user interface.
	 */
	gui();

	bool button(Rectangle rec, const char* text);
	bool image_button(Rectangle rec, Texture2D image);
	bool checkbox(Rectangle rec, const char* text, bool state);
	bool checkbox(Texture2D texture, Rectangle rec, bool state);
	void progress_bar(Texture2D texture, Rectangle rec, float scale, int value, int max);
	void n_times(Texture2D texture, float x, float y, float scale, int n_horizontal, int n_vertical);

private:
	/**
	 * spgu::ui::color_scheme for the color
	 */
	spgu::ui::color_scheme m_color_scheme;
	/**
	 * float for the roundness of the button
	 */
	float m_roundness;
};



#endif /* SRC_UTILS_INCLUDE_GUI_GUI_HPP_ */
