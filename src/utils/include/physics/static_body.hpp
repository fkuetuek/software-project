/*
 * static_body.hpp
 *
 *  Created on: Nov 22, 2021
 *      Author: ben
 */

#ifndef SRC_UTILS_INCLUDE_STATIC_BODY_HPP_
#define SRC_UTILS_INCLUDE_STATIC_BODY_HPP_

#include "spgu_shape.hpp"

namespace spgu {
namespace physics {
class static_body;
}
}
/**
 * @brief A class for defining static body
 * @author ysf
 *
 * This is a simple class for defining static body.
 */
class spgu::physics::static_body {
public:
	/**
	 * Constructor that creates the static body.
	 *
	 * @param body Shape of the body
	 */
	static_body(spgu::shape* body);

//	void set_body(float x, float y, float width, float height, b2World* world)
//	{
//		m_body = new spgu::shape::rectangle(x, y, width, height, world);
//	}

	spgu::shape* get_body();

private:
	/**
	 * spgu::shape* for the body shape
	 */
	 spgu::shape* m_body;

private:
};


#endif /* SRC_UTILS_INCLUDE_STATIC_BODY_HPP_ */
