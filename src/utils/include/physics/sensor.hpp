/*
 * sensor.hpp
 *
 *  Created on: Nov 22, 2021
 *      Author: ben
 */

#ifndef SRC_UTILS_INCLUDE_PHYSICS_SENSOR_HPP_
#define SRC_UTILS_INCLUDE_PHYSICS_SENSOR_HPP_

#include "box2d/box2d.h"
#include "spgu_shape.hpp"

namespace spgu {
namespace physics {
class sensor;
}
}
/**
 * @brief A class for defining sensor
 * @author ysf
 *
 * This is a simple class for creating sensor.
 */
class spgu::physics::sensor {
public:
	/**
	 * Constructor that creates the sensor.
	 *
	 * @param body Shape of the sensor
	 */
	sensor(spgu::shape* body);
	b2Body* get_body();
	spgu::shape* get_shape() {return m_body;}
	bool is_touching();

private:
	/**
	 * spgu::shape* for the shape of the sensor body
	 */
	spgu::shape* m_body;
};



#endif /* SRC_UTILS_INCLUDE_PHYSICS_SENSOR_HPP_ */
