/*
 * spgu_draw.cpp
 *
 *  Created on: Dec 21, 2021
 *      Author: yusuf
 */

#include "spgu_draw.hpp"

namespace spgu {

draw::draw(float map_scale, float animation_scale, int safety_row, int safety_col)
:	m_map_scale(map_scale),
	m_animation_scale(animation_scale),
	m_safety_row(safety_row),
	m_safety_col(safety_col)
{}

void draw::map(spgu::resources& res, spgu::map& map, std::string layer_name, b2Vec2 focus)
{
	int max_row = map.get_tile_layer(layer_name)->m_row;
	int max_col = map.get_tile_layer(layer_name)->m_column;
	float tile_size = map.get_tile_layer(layer_name)->m_tile_size;

	int row_start = -focus.y/tile_size/m_map_scale - m_safety_row;
	if (row_start < 0) row_start = 0;
	if (row_start > max_row) row_start = max_row;

	int row_end = -focus.y/tile_size/m_map_scale + m_safety_row;
	if (row_end < 0) row_end = 0;
	if (row_end > max_row) row_end = max_row;

	int col_start = focus.x/tile_size/m_map_scale - m_safety_col;
	if (col_start < 0) col_start = 0;
	if (col_start > max_col) col_start = max_col;

	int col_end = focus.x/tile_size/m_map_scale + m_safety_col;
	if (col_end < 0) col_end = 0;
	if (col_end > max_col) col_end = max_col;

	Texture2D tileset = res.get_texture("tileset");
//	for (unsigned int r = 0; r < map.get_tile_layer(layer_name)->m_row; ++r)
	for (int r = row_start; r < row_end; ++r)
	{
//		for (unsigned int c = 0; c < map.get_tile_layer(layer_name)->m_column; ++c)
		for (int c = col_start; c < col_end; ++c)
		{
			if (map.get_tile_layer(layer_name)->m_data[r][c] != 0)
			{
				int row = map.get_tile_layer(layer_name)->m_data[r][c]/map.m_col;
				int column = map.get_tile_layer(layer_name)->m_data[r][c]%map.m_col-1;
				DrawTexturePro(
						tileset,
						{
								(float) column*tile_size,
								(float) row*tile_size,
								tile_size,
								tile_size
						},
						{
								tile_size*c*m_map_scale,
								tile_size*r*m_map_scale,
								tile_size*m_map_scale,
								tile_size*m_map_scale
						},
						{0.0f, 0.0f},
						0.0f,
						{255, 255, 255, 255}
				);
			}
		}
	}
}

void draw::debug(b2World* world)
{
	Color clr = WHITE;
	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
	{
		const b2Transform& transform = b->GetTransform();
		for (b2Fixture* f = b->GetFixtureList(); f != nullptr; f = f->GetNext())
		{

			if (b->IsAwake())
			{
				clr = WHITE;
			}
			else
			{
				clr = MAGENTA;
			}
			if (f->GetType() == b2Shape::e_circle)
			{
				b2CircleShape* circle = (b2CircleShape*) f->GetShape();
				b2Vec2 center = b2Mul(transform, circle->m_p);
				float radius = circle->m_radius;
				DrawCircleLines(center.x, -center.y, radius, clr);
			}
			else if (f->GetType() == b2Shape::e_polygon)
			{
				b2PolygonShape* poly = (b2PolygonShape*) f->GetShape();
				int32 vertexCount = poly->m_count;
				b2Vec2 vertices[b2_maxPolygonVertices];
				float xv[b2_maxPolygonVertices];
				float yv[b2_maxPolygonVertices];
				for (int32 i = 0; i < vertexCount; ++i)
				{
					vertices[i] = b2Mul(transform, poly->m_vertices[i]);
					xv[i] = (float) vertices[i].x;
					yv[i] = (float) vertices[i].y;
				}
				for (int32 i = 0; i < vertexCount - 1; ++i)
				{
					DrawLine(xv[i], -yv[i], xv[i+1], -yv[i+1], clr);
				}
				DrawLine(xv[vertexCount-1], -yv[vertexCount-1], xv[0], -yv[0], clr);
			}
		}
		DrawCircle(transform.p.x, -transform.p.y, 2.0f, clr);
	}
}



void draw::animations(spgu::resources& res, entt::registry& registry)
{
//	float scale_factor = 2.0f;
	entt::basic_view view = registry.
			view<spgu::shape*, spgu::draw_quad, spgu::direction, spgu::animation_container, spgu::active_animation>();

	for (entt::entity e : view)
	{
		spgu::shape*& collider = view.get<spgu::shape*>(e);
		spgu::draw_quad& draw_quad = view.get<spgu::draw_quad>(e);
		spgu::direction& direction = view.get<spgu::direction>(e);
		spgu::animation_container& animations = view.get<spgu::animation_container>(e);
		spgu::active_animation& active_animation = view.get<spgu::active_animation>(e);
		b2Vec2 pos = collider->m_body->GetPosition();
		Vector2 frame_pos = {draw_quad.m_x, draw_quad.m_y};

		// decide animation direction
		int dir = 0;
		if (direction.m_direction.x == 0 && direction.m_direction.y < 0) dir = 0;
		else if (direction.m_direction.x > 0 && direction.m_direction.y < 0) dir = 1;
		else if (direction.m_direction.x > 0 && direction.m_direction.y == 0) dir = 2;
		else if (direction.m_direction.x > 0 && direction.m_direction.y > 0) dir = 3;
		else if (direction.m_direction.x == 0 && direction.m_direction.y > 0) dir = 4;
		else if (direction.m_direction.x < 0 && direction.m_direction.y > 0) dir = 5;
		else if (direction.m_direction.x < 0 && direction.m_direction.y == 0) dir = 6;
		else if (direction.m_direction.x < 0 && direction.m_direction.y < 0) dir = 7;
		else dir = 0;

		spgu::animation* anim = animations[active_animation.m_name];
		// update animation timer
		anim->m_current_time += GetFrameTime();
		if (anim->m_current_time > anim->m_speed)
		{

			if (anim->m_current_frame < anim->m_column - 1.0f)
			{
				anim->m_current_frame++;
			}
			else
			{
				anim->m_current_frame = 0;
			}
			anim->m_current_time = 0;
		}
		// draw the current animation frame
		DrawTexturePro
		(
				res.get_texture(active_animation.m_name),
				{
						(float) anim->m_current_frame*anim->m_tile_size,
						(float) dir*anim->m_tile_size,
						(float) anim->m_tile_size,
						(float) anim->m_tile_size
				},
				{
						frame_pos.x + draw_quad.m_offset_x - draw_quad.m_width*m_animation_scale/2.0f,
						frame_pos.y + draw_quad.m_offset_y - draw_quad.m_height*m_animation_scale/2.0f,//frame_size.y*m_animation_scale,
						draw_quad.m_width*m_animation_scale,
						draw_quad.m_height*m_animation_scale
//						(float) pos.x - anim->m_tile_size*m_animation_scale/2.0f,
//						(float) -pos.y - anim->m_tile_size*m_animation_scale/2.0f,
//						(float) anim->m_tile_size*m_animation_scale,
//						(float) anim->m_tile_size*m_animation_scale
				},
				{0.0f, 0.0f},
				0.0f,
				WHITE
		);
	}
}

}

