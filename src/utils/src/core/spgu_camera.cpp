/*
 * spgu_camera.cpp
 *
 *  Created on: Dec 22, 2021
 *      Author: christian
 */

#include "spgu_camera.hpp"

namespace spgu {


camera::camera(int window_width, int window_height)
{
	m_window_size = {(float) window_width, (float) window_height};
	m_camera.offset = (Vector2) {window_width/2.0f, window_height/2.0f};
	m_camera.rotation = 0.0f;
	m_camera.zoom = 1.0f;
	m_bounding_box_scale = {0.5f, 0.5f};
	m_bounding_box_min = {0.0f, 0.0f};
	m_bounding_box_max = {0.0f, 0.0f};
}

Camera2D camera::get()
{
	return m_camera;
}

void camera::set_zoom(float zoom)
{
	m_camera.zoom += zoom/10;
}

void camera::follow_centered(Vector2 target)
{
	m_camera.target = target;
}

void camera::push_camera(Vector2 target)
{
	m_bounding_box_min = GetScreenToWorld2D(
			(Vector2){(1 - m_bounding_box_scale.x)*0.5f*m_window_size.x, (1 - m_bounding_box_scale.y)*0.5f*m_window_size.y},
			m_camera
	);

	m_bounding_box_max = GetScreenToWorld2D(
			(Vector2){(1 + m_bounding_box_scale.x)*0.5f*m_window_size.x, (1 + m_bounding_box_scale.y)*0.5f*m_window_size.y},
			m_camera);

	m_camera.offset =
			(Vector2){(1 - m_bounding_box_scale.x)*0.5f*m_window_size.x, (1 - m_bounding_box_scale.y)*0.5f*m_window_size.y};

	if (target.x < m_bounding_box_min.x)
		m_camera.target.x = target.x;
	if (target.y < m_bounding_box_min.y)
		m_camera.target.y = target.y;
	if (target.x > m_bounding_box_max.x)
		m_camera.target.x = m_bounding_box_min.x + (target.x - m_bounding_box_max.x);
	if (target.y > m_bounding_box_max.y)
		m_camera.target.y = m_bounding_box_min.y + (target.y - m_bounding_box_max.y);
}

bool camera::is_seeing(Vector2 position)
{
	return
			position.x > m_camera.target.x - m_camera.offset.x &&
			position.x < m_camera.target.x + m_camera.offset.x &&
			position.y > m_camera.target.y - m_camera.offset.y &&
			position.y < m_camera.target.y + m_camera.offset.y;
}

void camera::reset_offset()
{
	m_camera.offset = (Vector2) { m_window_size.x/2.0f, m_window_size.y/2.0f };
}

}


