/*
 * rigid_body.cpp
 *
 *  Created on: Nov 22, 2021
 *      Author: angelov
 */

#include "rigid_body.hpp"

spgu::physics::rigid_body::rigid_body(spgu::shape* body)
:	m_body(body)
{
	m_body->m_body->SetType(b2_dynamicBody);
}

//spgu::shape::base* spgu::physics::rigid_body::get_body() {return m_body;}
b2Body* spgu::physics::rigid_body::get_body() {return m_body->m_body;}






