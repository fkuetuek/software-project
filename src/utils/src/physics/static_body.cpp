/*
 * static_body.cpp
 *
 *  Created on: Nov 22, 2021
 *      Author: angelov
 */

#include "static_body.hpp"

spgu::physics::static_body::static_body(spgu::shape* body)
:	m_body(body)
{
//	m_body->set_body_type(b2_staticBody);
}

spgu::shape* spgu::physics::static_body::get_body() {return m_body;}
