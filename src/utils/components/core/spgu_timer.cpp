/*
 * spgu_timer.cpp
 *
 *  Created on: Jan 2, 2022
 *      Author: angel
 */


#include "spgu_timer.hpp"

namespace spgu {

timer::timer(float limit) : m_limit(limit), m_current(0.0f) {}

}  // namespace spgu

