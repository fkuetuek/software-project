/*
 * spgu_texture.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: angel
 */

#ifndef SRC_UTILS_COMPONENTS_CORE_SPGU_TEXTURE_HPP_
#define SRC_UTILS_COMPONENTS_CORE_SPGU_TEXTURE_HPP_



#include "raylib.h"

namespace spgu {
/**
 * @brief A class for Textures
 * @author ysf
 *
 * This is a simple class for loading a texture to the game from a file.
 */

class texture {
public:
	/**
	 * Constructor that loads texture to the game from a file
	 *
	 * @param path Path to the texture file
	 */
	texture(const char* path);

public:
	/**
	 * Texture2D stuct
	 */
	Texture2D m_data;
};
}


#endif /* SRC_UTILS_COMPONENTS_CORE_SPGU_TEXTURE_HPP_ */
