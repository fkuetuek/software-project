/*
 * spgu_timer.hpp
 *
 *  Created on: Jan 2, 2022
 *      Author: angel
 */

#ifndef SRC_UTILS_COMPONENTS_CORE_SPGU_TIMER_HPP_
#define SRC_UTILS_COMPONENTS_CORE_SPGU_TIMER_HPP_

namespace spgu {
/**
 * @brief A struct for defining timer
 * @author ysf
 *
 * This is a simple struct for creating timer
 */
struct timer {
	/**
	 * Constructor that creates the timer.
	 *
	 * @param limit time limit for the timer in seconds
	 */
	timer(float limit);
	/**
	 * float for the time limit
	 */
	float m_limit;
	/**
	 * float for the current time
	 */
	float m_current;
};

}  // namespace spgu



#endif /* SRC_UTILS_COMPONENTS_CORE_SPGU_TIMER_HPP_ */
