/*
 * spgu_body.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: yusuf
 */

#ifndef SRC_UTILS_COMPONENTS_PHYSICS_SPGU_BODY_HPP_
#define SRC_UTILS_COMPONENTS_PHYSICS_SPGU_BODY_HPP_



#include "spgu_shape.hpp"

namespace spgu {
/**
 * @brief A template class which takes different shapes
 * @author yusuf
 *
 * This is a template class which takes different shapes.
 */
template<typename T>
class body {
public:
	body(T shape)
	:	m_shape(shape)
	{
	}

public:
	T m_shape;
};
}



#endif /* SRC_UTILS_COMPONENTS_PHYSICS_SPGU_BODY_HPP_ */
