/*
 * spgu_shape.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: yusuf
 */

#ifndef SRC_UTILS_COMPONENTS_PHYSICS_SPGU_SHAPE_HPP_
#define SRC_UTILS_COMPONENTS_PHYSICS_SPGU_SHAPE_HPP_

#include <iostream>
#include "box2d/box2d.h"

namespace spgu {

/**
 * @brief A base class for defining different shapes
 * @author ysf
 *
 * This is a simple class for creating different shapes with box2d.
 */

class shape {
public:
	/**
	 * Constructor that creates the shape.
	 *
	 * @param x Position in X coordinate
	 * @param y Position in Y coordinate
	 * @param type Body type of the shape
	 * @param world Pointer to box2d world that the shape is created in
	 */
	shape(float x, float y, b2BodyType type, b2World* world);
	virtual ~shape();

public:
	/**
	 * b2Body pointer that points to the shape
	 */
	b2Body* m_body;
};
}




#endif /* SRC_UTILS_COMPONENTS_PHYSICS_SPGU_SHAPE_HPP_ */
