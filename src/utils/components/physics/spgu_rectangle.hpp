/*
 * spgu_rectangle.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: christian
 */

#ifndef SRC_UTILS_COMPONENTS_PHYSICS_SPGU_RECTANGLE_HPP_
#define SRC_UTILS_COMPONENTS_PHYSICS_SPGU_RECTANGLE_HPP_


#include "box2d/box2d.h"
#include "spgu_shape.hpp"

namespace spgu {

/**
 * @brief A class for defining rectangle shape
 * @author ysf
 *
 * This is a simple class for creating rectangle shape with box2d.
 */

class rectangle : public spgu::shape {
public:
	/**
	 * Constructor that crates the rectangle shape.
	 *
	 * @param x Position in X coordinate
	 * @param y Position in Y coordinate
	 * @param width Width of the shape
	 * @param height Height of the shape
	 * @param type Body type of the shape
	 * @param world Pointer to box2d world that the shape is created in
	 */
	rectangle(float x, float y, float width, float height, b2BodyType type, b2World* world);
	virtual ~rectangle();
};
}




#endif /* SRC_UTILS_COMPONENTS_PHYSICS_SPGU_RECTANGLE_HPP_ */
