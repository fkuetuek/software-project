/*
 * spgu_speed.hpp
 *
 *  Created on: Jan 2, 2022
 *      Author: crystal
 */

#ifndef SRC_UTILS_COMPONENTS_PHYSICS_SPGU_SPEED_HPP_
#define SRC_UTILS_COMPONENTS_PHYSICS_SPGU_SPEED_HPP_


namespace spgu {
/**
 * @brief A struct for defining speed
 * @author ysf
 *
 * This is a simple struct for creating speed
 */
struct speed {
	/**
	 * Constructor that creates the speed.
	 *
	 * @param speed Speed
	 */
	speed(float speed);
	/**
	 * float for the speed
	 */
	float m_speed;
};

}


#endif /* SRC_UTILS_COMPONENTS_PHYSICS_SPGU_SPEED_HPP_ */
