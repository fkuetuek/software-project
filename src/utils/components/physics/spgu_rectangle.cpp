/*
 * spgu_rectangle.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: christian
 */

#include "spgu_rectangle.hpp"

namespace spgu {



rectangle::rectangle(float x, float y, float width, float height, b2BodyType type, b2World* world)
:	spgu::shape(x, y, type, world)
{
	b2PolygonShape box;
	box.SetAsBox(width/2.0f, height/2.0f);

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &box;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;

	m_body->CreateFixture(&fixtureDef);
}

rectangle::~rectangle() {}

}


