/*
 * spgu_box.hpp
 *
 *  Created on: Dec 28, 2021
 *      Author: fatma
 */

#ifndef SRC_UTILS_COMPONENTS_PHYSICS_SPGU_BOX_HPP_
#define SRC_UTILS_COMPONENTS_PHYSICS_SPGU_BOX_HPP_

#include "raylib.h"
namespace spgu {
/**
 * @brief A class for defining box
 * @author ysf
 *
 * This is a simple class for creating box with Raylib's Rectangle.
 */
struct box {
	/**
	 * Constructor that creates the box.
	 *
	 * @param x Position in X coordinate
	 * @param y Position in Y coordinate
	 * @param width Width of the rectangle
	 * @param height Width of the rectangle
	 */
	box(float x, float y, float width, float height);

	/**
	 * Rectangle for the box
	 */
	Rectangle m_box;

};


}  // namespace spgu



#endif /* SRC_UTILS_COMPONENTS_PHYSICS_SPGU_BOX_HPP_ */
