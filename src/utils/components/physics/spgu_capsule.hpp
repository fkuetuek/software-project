/*
 * spgu_capsule.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: christian
 */

#ifndef SRC_UTILS_COMPONENTS_PHYSICS_SPGU_CAPSULE_HPP_
#define SRC_UTILS_COMPONENTS_PHYSICS_SPGU_CAPSULE_HPP_


#include "box2d/box2d.h"
#include "spgu_shape.hpp"

namespace spgu {
/**
 * @brief A class for defining capsule shape
 * @author ysf
 *
 * This is a simple class for creating compound capsule shape with box2d.
 */

class capsule : public spgu::shape {
public:
	/**
	 * Constructor that creates the capsule shape.
	 *
	 * @param x Position in X coordinate
	 * @param y Position in Y coordinate
	 * @param width Width of the shape
	 * @param height Height of the shape
	 * @param horizontal Specify orientation of the shape
	 * @param type Body type of the shape
	 * @param world Pointer to box2d world that the shape is created in
	 */
	capsule(float x, float y, float width, float height, bool horizontal, b2BodyType type, b2World* world);

	virtual ~capsule();
};
}



#endif /* SRC_UTILS_COMPONENTS_PHYSICS_SPGU_CAPSULE_HPP_ */
