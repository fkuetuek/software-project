/*
 * spgu_animation.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: fatma
 */

#include "spgu_animation.hpp"

namespace spgu {

animation::animation
(
		unsigned int row,
		unsigned int column,
		unsigned int tile_size,
		float speed,
		bool repeat
)
:	m_row(row),
	m_column(column),
	m_tile_size(tile_size),
	m_speed(speed/column),
	m_current_time(0.0f),
	m_current_frame(0.0f),
	m_repeat(repeat)
{}

}  // namespace spgu
