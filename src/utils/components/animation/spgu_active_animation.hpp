/*
 * spgu_active_animation.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: fatma
 */

#ifndef SRC_UTILS_COMPONENTS_ANIMATION_SPGU_ACTIVE_ANIMATION_HPP_
#define SRC_UTILS_COMPONENTS_ANIMATION_SPGU_ACTIVE_ANIMATION_HPP_


#include <string>

namespace spgu {
/**
 * @brief A class for defining active animation
 * @author ysf
 *
 * This is a simple class for creating active animation.
 */
class active_animation {
public:
	/**
	 * Constructor that creates the active animation.
	 *
	 * @param name name of the active animation
	 */
	active_animation(std::string name);

public:
	/**
	 * std::string for the name of the active animation
	 */
	std::string m_name;
};

}



#endif /* SRC_UTILS_COMPONENTS_ANIMATION_SPGU_ACTIVE_ANIMATION_HPP_ */
