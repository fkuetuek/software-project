/*
 * spgu_animation.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: fatma
 */

#ifndef SRC_UTILS_COMPONENTS_ANIMATION_SPGU_ANIMATION_HPP_
#define SRC_UTILS_COMPONENTS_ANIMATION_SPGU_ANIMATION_HPP_


#include <string>

/**
 * @brief A name space for the core
 * 
 *
 * This is a name space for the core.
 */
namespace spgu {
/**
 * @brief A class for defining animation
 * @author ysf
 *
 * This is a simple class for creating animation.
 */
class animation {
public:
	/**
	 * Constructor defines the number of rows and columns frames
	 * of a sprite sheet file, speed of the animation, and tile size of
	 * the sprite.
	 *
	 * @param row number of horizontal frames in the sprite sheet file
	 * @param column number of vertical frames in the sprite sheet file
	 * @param tile_size size of a tile in the sprite sheet file
	 * @param speed Speed of the animation
	 * @param repeat if animation repeats
	 */
	animation
	(
			unsigned int row,
			unsigned int column,
			unsigned int tile_size,
			float speed,
			bool repeat
	);

public:
	/**
	 * unsigned int for the number of the rows in the sprite sheet
	 */
	unsigned int m_row;
	/**
	 * unsigned int for the number of the columns in the sprite sheet
	 */
	unsigned int m_column;
	/**
	 * unsigned int for the tile size of a tile in the sprite sheet
	 */
	unsigned int m_tile_size;
	/**
	 * float for the animation speed
	 */
	float m_speed;
	/**
	 * float for the current time
	 */
	float m_current_time;
	/**
	 * float for the current frame
	 */
	float m_current_frame;
	/**
	 * bool for the animation repeat
	 */
	bool m_repeat;
};
}




#endif /* SRC_UTILS_COMPONENTS_ANIMATION_SPGU_ANIMATION_HPP_ */
