/*
 * spgu_animation_container.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: fatma
 */

#include "spgu_animation_container.hpp"

namespace spgu {

// TODO spgu::animation&
spgu::animation* animation_container::operator[](std::string name)
{
	return m_animations[name];
}

}


