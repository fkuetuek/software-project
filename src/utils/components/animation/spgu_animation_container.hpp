/*
 * spgu_animation_container.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: fatma
 */

#ifndef SRC_UTILS_COMPONENTS_ANIMATION_SPGU_ANIMATION_CONTAINER_HPP_
#define SRC_UTILS_COMPONENTS_ANIMATION_SPGU_ANIMATION_CONTAINER_HPP_

#include <map>
#include <string>

#include "spgu_animation.hpp"


namespace spgu {
/**
 * @brief A class for defining animation container
 * @author ysf
 *
 * This is a simple class for creating animation container.
 */
class animation_container {
public:
	spgu::animation* operator[](std::string name);

public:
	/**
	 * std::map<std::string, spgu::animation*> for the animations
	 */
	std::map<std::string, spgu::animation*> m_animations;
};

}  // namespace spgu





#endif /* SRC_UTILS_COMPONENTS_ANIMATION_SPGU_ANIMATION_CONTAINER_HPP_ */
