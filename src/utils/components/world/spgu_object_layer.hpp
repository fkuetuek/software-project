/*
 * spgu_object_layer.hpp
 *
 *  Created on: Dec 21, 2021
 *      Author: yusuf
 */

#ifndef SRC_UTILS_COMPONENTS_WORLD_SPGU_OBJECT_LAYER_HPP_
#define SRC_UTILS_COMPONENTS_WORLD_SPGU_OBJECT_LAYER_HPP_


#include <iostream>
#include <vector>

#include "json.hpp"
#include "box2d/box2d.h"

#include "spgu_shape.hpp"
#include "spgu_rectangle.hpp"
#include "spgu_circle.hpp"
#include "spgu_capsule.hpp"

namespace spgu {
/**
 * @brief A class for importing object layer from "Tiled" map
 * @author ysf
 *
 * This is a simple class for importing object layer from "Tiled" map
 */

class object_layer {
public:
	/**
	 * Constructor that imports the object layer from a JSON object.
	 *
	 * @param json Reference to json object
	 * @param layer_index Index of the layer
	 * @param world Pointer to box2d world that the shapes on the object layer is created in
	 */
	object_layer(const nlohmann::json& json, int layer_index, b2World* world, float scale);
	~object_layer();


public:
	/**
	 * vector of Pointers to comp_shapes
	 */
	std::vector<spgu::shape*> m_data;
};

}


#endif /* SRC_UTILS_COMPONENTS_WORLD_SPGU_OBJECT_LAYER_HPP_ */
