/*
 * spgu_map_layer.hpp
 *
 *  Created on: Dec 24, 2021
 *      Author: ben
 */

#ifndef SRC_UTILS_COMPONENTS_WORLD_SPGU_MAP_LAYER_HPP_
#define SRC_UTILS_COMPONENTS_WORLD_SPGU_MAP_LAYER_HPP_

#include <iostream>
#include "json.hpp"
#include "box2d/box2d.h"

namespace spgu {
/**
 * @brief A template class for map layers.
 * @author yusuf
 *
 * This is a class which for map layers.
 */
template <typename T>
struct map_layer {
	map_layer(const nlohmann::json& json, T layer, int layer_index, b2World* world)
	{

	}
};

}

#endif /* SRC_UTILS_COMPONENTS_WORLD_SPGU_MAP_LAYER_HPP_ */
