/*
 * spgu_map.cpp
 *
 *  Created on: Dec 21, 2021
 *      Author: ben
 */

#include "spgu_map.hpp"

namespace spgu {

map::map(): m_col(0), m_scale(1.0f) {}

map::map(const nlohmann::json& map_json, b2World* world, int col, float scale)
:	m_col(col), m_scale(scale)
{
	int layer_count = map_json["layers"].size();
	//	int tile_size = map_json["tilewidth"];

	for (int i = 0; i < layer_count; ++i)
	{
		// "type":"objectgroup"
		if (map_json["layers"][i]["type"] == "tilelayer")
		{
			m_map.emplace(
					map_json["layers"][i]["name"],
					new spgu::tile_layer(map_json, i)
			);
		}
		else if (map_json["layers"][i]["type"] == "objectgroup")
		{
			m_map.emplace(
					map_json["layers"][i]["name"],
					new spgu::object_layer(map_json, i, world, m_scale)
			);
		}
//		else if (map_json["layers"][i]["type"] == "entity")
//		{
//		}
	}
}

map::map(const char* path, b2World* world, int col)
:	m_col(col), m_scale(1.0f)
{
	std::ifstream map_file(path);
	nlohmann::json map_json;
	map_file >> map_json;

	int layer_count = map_json["layers"].size();
	//	int tile_size = map_json["tilewidth"];

	for (int i = 0; i < layer_count; ++i)
	{
		// "type":"objectgroup"
		if (map_json["layers"][i]["type"] == "tilelayer")
		{
			m_map.emplace(
					map_json["layers"][i]["name"],
					new spgu::tile_layer(map_json, i)
			);
		}
		else if (map_json["layers"][i]["type"] == "objectgroup")
		{
			m_map.emplace(
					map_json["layers"][i]["name"],
					new spgu::object_layer(map_json, i, world, m_scale)
			);
		}
//		else if (map_json["layers"][i]["type"] == "generic")
//		{
//		}
	}
	//	m_texture = LoadTexture(texture_path);
	//	m_horizontal_tile_count = m_texture.width/tile_size;

}

spgu::tile_layer* map::get_tile_layer(std::string name)
{
	return std::get<spgu::tile_layer*>(m_map[name]);
}

spgu::object_layer* map::get_object_layer(std::string name)
{
	return std::get<spgu::object_layer*>(m_map[name]);
}


map::~map()
{
	// TODO not use std::visit
	for
	(
			std::map<std::string,
			std::variant<spgu::tile_layer*, spgu::object_layer*>>::iterator itr = m_map.begin();
			itr != m_map.end(); itr++
	)
	{
		std::visit([] (auto& arg) {
			using T = std::decay_t<decltype(arg)>;

			if (std::is_same_v<T, spgu::tile_layer*>)
			{
				delete arg;
			}
			else if (std::is_same_v<T, spgu::object_layer*>)
			{
				delete arg;
			}

		}, itr->second);

	}

	m_map.clear();
}


}


