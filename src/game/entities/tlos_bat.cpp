/*
 * tlos_bat.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: angelov
 */

#include "tlos_bat.hpp"

namespace tlos {

void bat::create(entt::registry& registry, b2World* world, float x, float y)
{
	entt::entity ent = registry.create();
	registry.emplace<tlos::entity_type>(ent, tlos::type::enemy);
	registry.emplace<tlos::entity_state>(ent, tlos::state::idle);
	registry.emplace<spgu::shape*>(ent, new spgu::circle(x, y, 50.0f, b2_dynamicBody, world));
	registry.emplace<tlos::speed>(ent, 100);
	registry.emplace<tlos::sight>(ent, 500);
	registry.emplace<tlos::character_stats>(ent, 5, 1000, 250);
	registry.emplace<spgu::draw_quad>(ent, 64.0f, 64.0f, 0.0f, -50.0f);
	registry.emplace<tlos::hitbox>(ent, 75.0f, 75.0f, 50.0f, 75.0f);
	registry.emplace<tlos::hurtbox>(ent, 100.0f, 150.0f, 0.0f, 0.0f);
	registry.emplace<spgu::direction>(ent, 0.0f, 1.0f);
	registry.emplace<spgu::active_animation>(ent, "bat_fly");
	spgu::animation_container& animation_container = registry.emplace<spgu::animation_container>(ent);
	animation_container.m_animations.emplace("bat_fly", new spgu::animation(8, 4, 48, 1.0f, true));
	registry.emplace<tlos::bat_type>(ent);
}

}


