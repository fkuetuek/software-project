/*
 * tlos_boss.cpp
 *
 *  Created on: Jan 4, 2022
 *      Author: gao, angelov
 */

#include "tlos_boss.hpp"

namespace tlos {

void boss::create(entt::registry& registry, b2World* world, float x, float y)
{
	entt::entity ent = registry.create();
	registry.emplace<tlos::entity_type>(ent, tlos::type::boss);
	registry.emplace<tlos::entity_state>(ent, tlos::state::idle);
	registry.emplace<spgu::shape*>(ent, new spgu::circle(x, y, 120.0f, b2_dynamicBody, world));
	registry.emplace<tlos::hitbox>(ent, 128.0f, 192.0f, 128.0f, 64.0f);
	registry.emplace<tlos::hurtbox>(ent, 148.0f, 256.0f, 0.0f, 96.0f);
	registry.emplace<spgu::direction>(ent, 0.0f, -1.0f);
	registry.emplace<tlos::character_stats>(ent, 10, 1000, 250);
	registry.emplace<tlos::speed>(ent, 75);
	registry.emplace<tlos::sight>(ent, 1000);
	registry.emplace<spgu::draw_quad>(ent, 192.0f, 192.0f, 0.0f, -100.0f);
	registry.emplace<spgu::active_animation>(ent, "wn_idle");
	spgu::animation_container& animation_container = registry.emplace<spgu::animation_container>(ent);
	animation_container.m_animations.emplace("wn_idle", new spgu::animation(8, 4, 192, 1.0f, true));
	animation_container.m_animations.emplace("wn_walk", new spgu::animation(8, 6, 192, 0.75f, true));
	animation_container.m_animations.emplace("wn_attack", new spgu::animation(8, 4, 192, 0.4f, false));
	registry.emplace<tlos::boss_type>(ent);
}

}

