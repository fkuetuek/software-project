/*
 * tlos_lava.cpp
 *
 *  Created on: Dec 21, 2021
 *      Author: ben
 */




#include "tlos_lava.hpp"

namespace tlos {

lava::lava(entt::registry& registry, b2World* world)
{
	entt::entity ent = registry.create();
	registry.emplace<tlos::entity_type>(ent, tlos::type::enemy);
	registry.emplace<tlos::entity_state>(ent, tlos::state::spawn);
	registry.emplace<spgu::shape*>
	(
			ent,
			new spgu::circle
			(
					(float) GetRandomValue(0, 2750),
					(float) GetRandomValue(0, 2750),
					75.0f, b2_dynamicBody, world
			)
	);
//	registry.emplace<tlos::hitbox>(ent, 0.0f, 0.0f, 50.0f, 75.0f);
	registry.emplace<spgu::direction>(ent, 0.0f, -1.0f);
	registry.emplace<spgu::active_animation>(ent, "lava_walk");
	spgu::animation_container& animation_container = registry.emplace<spgu::animation_container>(ent);
	animation_container.m_animations.emplace("lava_spawn", new spgu::animation(8, 6, 96, 3.0f, true));
	animation_container.m_animations.emplace("lava_walk", new spgu::animation(8, 6, 96, 3.0f, true));
	animation_container.m_animations.emplace("lava_attack", new spgu::animation(8, 7, 96, 0.5f, false));

}

}

