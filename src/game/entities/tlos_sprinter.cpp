/*
 * tlos_sprinter.cpp
 *
 *  Created on: Jan 2, 2022
 *      Author: gao
 */



/*
 * tlos_bat.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: ben
 */

#include "tlos_sprinter.hpp"

namespace tlos {

void sprinter::create(entt::registry& registry, b2World* world, float x, float y)
{
	entt::entity ent = registry.create();
	registry.emplace<tlos::entity_type>(ent, tlos::type::enemy);
	registry.emplace<tlos::entity_state>(ent, tlos::state::idle);
	registry.emplace<spgu::shape*>(ent, new spgu::circle(x, y, 75.0f, b2_dynamicBody, world));
	registry.emplace<tlos::character_stats>(ent, 5, 1000, 250);
	registry.emplace<tlos::speed>(ent, 300);
	registry.emplace<tlos::sight>(ent, 300);
	registry.emplace<spgu::draw_quad>(ent, 64.0f, 64.0f, 0.0f, -20.0f);
	registry.emplace<tlos::hitbox>(ent, 75.0f, 75.0f, 50.0f, 75.0f);
	registry.emplace<tlos::hurtbox>(ent, 100.0f, 150.0f, 0.0f, 0.0f);
	registry.emplace<spgu::direction>(ent, 0.0f, 1.0f);
	registry.emplace<spgu::active_animation>(ent, "sprinter_spin");
	spgu::animation_container& animation_container = registry.emplace<spgu::animation_container>(ent);
	animation_container.m_animations.emplace("sprinter_spin", new spgu::animation(8, 6, 48, 1.0f, true));
	registry.emplace<tlos::sprinter_type>(ent);
}

}



