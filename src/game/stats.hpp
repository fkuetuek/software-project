/*
 * stats.hpp
 *
 *  Created on: November 27, 2021
 *      Author: fsk
 */

#ifndef SOFTWARE_PROJECT_NEW_SRC_GAME_STATS_HPP_
#define SOFTWARE_PROJECT_NEW_SRC_GAME_STATS_HPP_
#include <string>

int score(int n);

int max_health();

int health(int n);

int game_result(int lives);

int game_state(std::string state);





#endif /* SOFTWARE_PROJECT_NEW_SRC_GAME_STATS_HPP_ */
