/*
 * tlos_sop_controller.cpp
 *
 *  Created on: Dec 26, 2021
 *      Author: nasim, christian
 */

#include "tlos_sop_controller.hpp"

namespace tlos {

void sop_controller::decide_animation(entt::registry& registry)
{
	entt::basic_view view = registry.view<
			spgu::active_animation,
			tlos::entity_state,
			tlos::player_type
			>();//,

	for (entt::entity e : view)
	{
		spgu::active_animation& active_animation = view.get<spgu::active_animation>(e);
		tlos::entity_state& entity_state = view.get<tlos::entity_state>(e);

		if (entity_state.m_state == tlos::state::idle) {active_animation.m_name = "sop_idle";}
		else if (entity_state.m_state == tlos::state::walk) {active_animation.m_name = "sop_walk";}
		else if (entity_state.m_state == tlos::state::attack) {active_animation.m_name = "sop_attack";}
		else if (entity_state.m_state == tlos::state::roll) {active_animation.m_name = "sop_roll";}
	}
}

}  // namespace tlos


