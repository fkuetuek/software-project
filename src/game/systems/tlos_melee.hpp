/*
 * tlos_melee.hpp
 *
 *  Created on: Dec 23, 2021
 *      Author: gao, angelov, angel
 */

#ifndef SRC_GAME_SYSTEMS_TLOS_MELEE_HPP_
#define SRC_GAME_SYSTEMS_TLOS_MELEE_HPP_

#include <iostream>
#include "raylib.h"
#include "entt.hpp"
#include "box2d/box2d.h"
#include "config.hpp"
#include "tlos_sop.hpp"
#include "tlos_bat.hpp"
#include "tlos_lava.hpp"
#include "tlos_boss.hpp"

// systems
#include "tlos_movement.hpp"
#include "tlos_player_state.hpp"
#include "tlos_player_type.hpp"
#include "tlos_bat_type.hpp"
#include "tlos_character_stats.hpp"
#include "tlos_diamond.hpp"
#include "tlos_game_controller.hpp"


namespace tlos {
/**
 * @brief A class which provides attack and take damage functionality
 * @author gao, angelov, angel
 *
 * This is a class which provides attack and take damage functionality.
 */
struct melee {
	void attack(entt::registry& registry);
	void take_damage(entt::registry& registry);
};

}




#endif /* SRC_GAME_SYSTEMS_TLOS_MELEE_HPP_ */
