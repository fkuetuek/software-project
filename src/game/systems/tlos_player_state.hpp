/*
 * tlos_player_state.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: nasim, christian
 */

#ifndef SRC_GAME_SYSTEMS_TLOS_PLAYER_STATE_HPP_
#define SRC_GAME_SYSTEMS_TLOS_PLAYER_STATE_HPP_



#include <iostream>
#include "box2d/box2d.h"
#include "raylib.h"
#include "tlos_entity_type.hpp"
#include "tlos_entity_state.hpp"
#include "spgu_direction.hpp"
#include "entt.hpp"
#include "tlos_player_type.hpp"

namespace tlos {
/**
 * @brief A class which determines the state of the player
 * @author nasim, christian
 *
 * This is a class which determines the state of the player.
 */
class player_state {
public:
	player_state();
	void update(entt::registry& registry);

private:
	void idle(tlos::entity_state& state);
	void walk(tlos::entity_state& state);
	void attack(tlos::entity_state& state);
	void roll(tlos::entity_state& state);

public:
	float m_time;
	float m_attack_cooldown;
	float m_roll_cooldown;

};

}



#endif /* SRC_GAME_SYSTEMS_TLOS_PLAYER_STATE_HPP_ */
