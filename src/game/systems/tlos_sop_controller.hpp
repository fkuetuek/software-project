/*
 * tlos_sop_controller.hpp
 *
 *  Created on: Dec 26, 2021
 *      Author: nasim, christian
 */

#ifndef SRC_GAME_SYSTEMS_TLOS_SOP_CONTROLLER_HPP_
#define SRC_GAME_SYSTEMS_TLOS_SOP_CONTROLLER_HPP_

#include "entt.hpp"
#include "spgu_active_animation.hpp"
#include "tlos_entity_state.hpp"
#include "tlos_entity_type.hpp"
#include "tlos_player_type.hpp"

namespace tlos {
/**
 * @brief A class which determines the animation
 * @author nasim, christian
 *
 * This is a class which determines the animation.
 */
struct sop_controller {
	static void decide_animation(entt::registry& registry);
};

}


#endif /* SRC_GAME_SYSTEMS_TLOS_SOP_CONTROLLER_HPP_ */
