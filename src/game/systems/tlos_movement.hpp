/*
 * movement.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: nasim, christian
 */

#ifndef SRC_GAME_SYSTEMS_TLOS_MOVEMENT_HPP_
#define SRC_GAME_SYSTEMS_TLOS_MOVEMENT_HPP_



#include <iostream>
#include "entt.hpp"
#include "raylib.h"

#include "tlos_sop.hpp"
#include "tlos_entity_state.hpp"
#include "tlos_entity_type.hpp"
#include "spgu_circle.hpp"
#include "spgu_shape.hpp"
#include "spgu_draw_quad.hpp"
#include "tlos_hitbox.hpp"
#include "tlos_sprinter_type.hpp"
#include "tlos_bat_type.hpp"
#include "tlos_speed.hpp"
#include "tlos_sight.hpp"


namespace tlos {
/**
 * @brief A class which provides move and follow functionality
 * @author nasim, christian
 *
 * This is a class which provides move and follow functionality.
 */
class movement {
public:
	void move(entt::registry& registry);
	void follow_player(entt::registry& registry);

public:
	b2Vec2 sop_pos;
};

}


#endif /* SRC_GAME_SYSTEMS_MOVEMENT_HPP_ */
