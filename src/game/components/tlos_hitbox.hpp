/*
 * tlos_hitbox.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: angelov
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_HITBOX_HPP_
#define SRC_GAME_COMPONENTS_TLOS_HITBOX_HPP_

#include <iostream>
#include "raylib.h"

namespace tlos {
/**
 * @brief A struct for defining hitbox
 * @author gao
 *
 * This is a simple struct for defining hitbox.
 */
struct hitbox {
	hitbox(float width, float height, float offset_x, float offset_y);
	~hitbox();

	Rectangle m_hitbox;
	Vector2 m_offset;
};

}



#endif /* SRC_GAME_COMPONENTS_TLOS_HITBOX_HPP_ */
