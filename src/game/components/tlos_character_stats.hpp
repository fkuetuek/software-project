/*
 * tlos_character_stats.hpp
 *
 *  Created on: Dec 22, 2021
 *      Author: angelov
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_CHARACTER_STATS_HPP_
#define SRC_GAME_COMPONENTS_TLOS_CHARACTER_STATS_HPP_

namespace tlos {
/**
 * @brief A class for defining character status
 * @author angelov
 *
 * This is a simple class for defining the character status.
 */
struct character_stats {

	character_stats
	(
			int lives,
			int health,
			int attack
	);
	character_stats
	(
			int level,
			int experience,
			int experience_next,
			int lives,
			int max_health,
			int health,
			int attack
	);

	int m_level;
	int m_experience;
	int m_experience_next;
	int m_lives;
	int m_max_health;
	int m_health;
	int m_attack;
	int m_score;


};

}



#endif /* SRC_GAME_COMPONENTS_TLOS_CHARACTER_STATS_HPP_ */
