/*
 * tlos_player_type.hpp
 *
 *  Created on: Dec 23, 2021
 *      Author: nasim, christian
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_PLAYER_TYPE_HPP_
#define SRC_GAME_COMPONENTS_TLOS_PLAYER_TYPE_HPP_

namespace tlos {
/**
 * @brief A struct for defining player type
 * @author nasim, christian
 *
 * This is a simple struct for defining player type.
 */
struct player_type {
	player_type();
private:
	char m_c;
};

}  // namespace tlos




#endif /* SRC_GAME_COMPONENTS_TLOS_PLAYER_TYPE_HPP_ */
