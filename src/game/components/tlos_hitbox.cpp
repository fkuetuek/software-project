/*
 * tlos_hitbox.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: angelov
 */

#include "tlos_hitbox.hpp"

namespace tlos {

hitbox::hitbox(float width, float height, float offset_x, float offset_y)
:	m_hitbox({0.0f, 0.0f, width, height}), m_offset({offset_x, offset_y})
{}

hitbox::~hitbox()
{}

}


