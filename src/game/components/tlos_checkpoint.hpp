/*
 * tlos_check_point.hpp
 *
 *  Created on: Dec 28, 2021
 *      Author: fatma
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_CHECKPOINT_HPP_
#define SRC_GAME_COMPONENTS_TLOS_CHECKPOINT_HPP_

#include "entt.hpp"
#include "spgu_box.hpp"
#include "spgu_point_order.hpp"

namespace tlos {
/**
 * @brief A class for defining check point
 * @author fatma
 *
 * This is a simple class for defining a check point.
 */
struct checkpoint {
	static void create(entt::registry& registry, float x, float y, float width, float height, int order);
};


}  // namespace tlos


#endif /* SRC_GAME_COMPONENTS_TLOS_CHECKPOINT_HPP_ */
