/*
 * tlos_bonus_item_type.hpp
 *
 *  Created on: Jan 4, 2022
 *      Author: angel
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_BONUS_ITEM_TYPE_HPP_
#define SRC_GAME_COMPONENTS_TLOS_BONUS_ITEM_TYPE_HPP_

namespace tlos {

enum struct item_type {
	diamond,
	controller
};
/**
 * @brief A class for defining bonus item type
 * @author angel
 *
 * This is a simple class for defining the specific bonus item type.
 */

struct bonus_item_type {
	bonus_item_type(tlos::item_type type);

	tlos::item_type m_type;
};

}

#endif /* SRC_GAME_COMPONENTS_TLOS_BONUS_ITEM_TYPE_HPP_ */
