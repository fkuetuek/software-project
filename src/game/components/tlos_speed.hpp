/*
 * tlos_speed.hpp
 *
 *  Created on: Dec 21, 2021
 *      Author: nasim
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_SPEED_HPP_
#define SRC_GAME_COMPONENTS_TLOS_SPEED_HPP_

namespace tlos {
/**
 * @brief A struct for defining speed
 * @author nasim
 *
 * This is a simple struct for defining speed.
 */
struct speed {
	speed(float speed);

	float m_speed;
};

}  // namespace tlos



#endif /* SRC_GAME_COMPONENTS_TLOS_SPEED_HPP_ */
