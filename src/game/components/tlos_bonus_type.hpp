/*
 * tlos_bonus_type.hpp
 *
 *  Created on: Jan 1, 2022
 *      Author: angel
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_BONUS_TYPE_HPP_
#define SRC_GAME_COMPONENTS_TLOS_BONUS_TYPE_HPP_

namespace tlos {
/**
 * @brief A class for defining bonus type
 * @author angel
 *
 * This is a simple class for defining the specific bonus type.
 */
struct bonus_type {
	bonus_type(int value);
	int m_value;
};

}  // namespace tlos



#endif /* SRC_GAME_COMPONENTS_TLOS_BONUS_TYPE_HPP_ */
