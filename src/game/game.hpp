#ifndef SRC_GAME_GAME_HPP_
#define SRC_GAME_GAME_HPP_

// std
#ifdef __linux__
#include <filesystem>
#endif

#include <iostream>
// external
#include "entt.hpp"
#include "box2d/box2d.h"

// spgu
#include "spgu_camera.hpp"
#include "spgu_draw.hpp"
#include "spgu_map.hpp"
#include "spgu_resources.hpp"
#include "spgu_story.hpp"
#include "spgu_point_order.hpp"
#include "spgu_timer.hpp"
#include "gui.hpp"

// tlos
#include "config.hpp"
#include "tlos_sop.hpp"
#include "tlos_bat.hpp"
#include "tlos_movement.hpp"
#include "tlos_player_state.hpp"
#include "tlos_boss_state.hpp"
#include "tlos_player_type.hpp"
#include "tlos_bat_type.hpp"
#include "tlos_melee.hpp"
#include "tlos_entity_importer.hpp"
#include "tlos_get_sop.hpp"
#include "tlos_sop_controller.hpp"
#include "tlos_draw.hpp"
#include "tlos_diamond.hpp"
#include "tlos_checkpoint.hpp"
#include "tlos_character_stats.hpp"

//
/**
 * @brief A name space for the game
 * 
 *
 * This is a name space for the game.
 */
namespace tlos {

enum class game_state {
	running,
	paused,
	story,
	help,
	close,
	win,
	game_over
};
class game;
}
/**
 * @brief A class for the main game logic
 * 
 *
 * This is a class for the main game logic.
 */
class tlos::game {
public:
	game();
	~game();
	void init_resources();
	void reset();
	void update();
	void draw();
	void run();
	void running();
	void paused();
	void story();
	void help();
	void win();
	void game_over();
	void init_checkpoints();
	void help_map();
	void bonus();
	void update_health();
	void sop_win();

private:
	void play_audio();
private:
	entt::registry m_registry;
	b2World m_world;
	tlos::game_state m_state;
	spgu::camera m_camera;
	spgu::ui::gui m_gui;
	spgu::map* m_map;
	spgu::resources m_res;
	spgu::draw m_draw;
	tlos::movement m_movement;
	tlos::player_state m_player_state;
	tlos::boss_state m_boss_state;
	tlos::melee m_melee;
	bool m_muted;
	spgu::story m_story;
	b2Vec2 m_sop_pos;
	Vector2 m_check_points[19];
	std::string* m_wd;
//	std::vector<Vector2> m_check_points;
};

#endif
