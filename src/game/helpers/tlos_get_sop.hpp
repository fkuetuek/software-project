/*
 * tlos_get_sop.hpp
 *
 *  Created on: Dec 24, 2021
 *      Author: christian
 */

#ifndef SRC_GAME_HELPERS_TLOS_GET_SOP_HPP_
#define SRC_GAME_HELPERS_TLOS_GET_SOP_HPP_

#include "entt.hpp"
#include "box2d/box2d.h"

#include "spgu_shape.hpp"
#include "tlos_player_type.hpp"

namespace tlos {
/**
 * @brief A class which gets player's position
 * @author christian
 *
 * This is a class which gets player's position.
 */
struct get_sop {

	static b2Vec2 position(entt::registry& registry);

};

}  // namespace tlos


#endif /* SRC_GAME_HELPERS_TLOS_GET_SOP_HPP_ */
