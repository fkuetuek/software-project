/*
 * tlos_draw.hpp
 *
 *  Created on: Dec 30, 2021
 *      Author: christian
 */

#ifndef SRC_GAME_HELPERS_TLOS_DRAW_HPP_
#define SRC_GAME_HELPERS_TLOS_DRAW_HPP_

#include "entt.hpp"
#include "raylib.h"
#include <iostream>

#include "spgu_draw_quad.hpp"
#include "tlos_hitbox.hpp"
#include "tlos_hurtbox.hpp"

namespace tlos {
/**
 * @brief A debug drawer for entities
 * @author christian
 *
 * This is a debug drawer for entities.
 */
struct draw {
	static void debug(entt::registry& registry);
};

}



#endif /* SRC_GAME_HELPERS_TLOS_DRAW_HPP_ */
