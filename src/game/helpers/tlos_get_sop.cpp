/*
 * tlos_get_sop.cpp
 *
 *  Created on: Dec 24, 2021
 *      Author: christian
 */

#include "tlos_get_sop.hpp"

namespace tlos {

b2Vec2 get_sop::position(entt::registry& registry)
{
	entt::basic_view view = registry.view<spgu::shape*, tlos::player_type>();

	b2Vec2 pos = b2Vec2_zero;
	for (entt::entity e : view)
	{
		spgu::shape*& collider = view.get<spgu::shape*>(e);
		pos = collider->m_body->GetPosition();
	}
	return pos;
}

}


