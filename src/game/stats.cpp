/*
 * stats.cpp
 *
 *  Created on: November 27, 2021
 *      Author: fsk
 */
#include "stats.hpp"


int score(int n)
{
	return n*2;
}

int max_health()
{
	return 5;
}

int health(int n)
{
	if (n>5)
	{
		return 5;
	}
	else
	{
		return n;
	}
}

int game_result(int lives)
{
    if(lives > 0) return 0;
    return 1;  
}

int game_state(std::string state)
{
    if ("menu" == state) return 0;
    if ("run" == state) return 1;
    if ("game_over" == state) return 2;
    if ("win" == state) return 3;
    return -1;
}
