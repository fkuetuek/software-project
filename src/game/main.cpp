#include "raylib.h"
#include "spgu_window.hpp"
#include "config.hpp"
#include "game.hpp"
/**
 * @brief Entry point for the game
 * @author nasim, christian
 *
 * This is an entry point for the game.
 */
int main()
{
	spgu::window window(WINDOW_WIDTH, WINDOW_HEIGHT, GAME_NAME);
	tlos::game game;
	game.run();

	return 0;
}



